package com.example.leaderelection.controller;

import org.apache.zookeeper.KeeperException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.leaderelection.constants.Status;
import com.example.leaderelection.service.ZookeeperLeaderService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class LeaderController extends GenericController  {
	
	
	
	@Autowired
	ZookeeperLeaderService zookeeperLeaderService;
	
	@GetMapping("/current-leader")
	public String getCurrentLeader() throws KeeperException, InterruptedException {
		String currentLeader = zookeeperLeaderService.getCurrentLeader();
		log.info("Current Leader is {}",currentLeader);
		return currentLeader;
	}
	
	
	@PostMapping("/volunter-for-leadership")
	public String applyForLeaderShip() throws KeeperException, InterruptedException { 
		zookeeperLeaderService.volunterForLeadership();
		log.info("Voluteer request accepted successfully.");
		return Status.SUCCESS.toString();
	}

}
