package com.example.leaderelection.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.leaderelection.constants.ErrorCode;
import com.example.leaderelection.constants.Status;
import com.example.leaderelection.exceptions.LeaderServiceException;
import com.example.leaderelection.model.GenericResponse;

/**
 * This class handles exception in a centralised fashion , and returns a error to the caller in a generic format.
 * 
 * @author aman.choudhary
 * @version 1.0.0
 * @since 05-07-2021
 * 
 * **/

public abstract class GenericController {
	
	@ResponseStatus(code = HttpStatus.CONFLICT)
	@ExceptionHandler(LeaderServiceException.class)
	public GenericResponse handleServiceException(HttpServletRequest req,Exception ex) {
		
		GenericResponse response = new GenericResponse();
		response.setStatus(Status.FAILURE);
		response.setErrorCode(((LeaderServiceException)ex).getErrorCode());
		response.setErrorMessage(((LeaderServiceException)ex).getErrorMessage());
		return response;
	}
	
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public GenericResponse handleGenericException(HttpServletRequest req,Exception ex) {
		
		GenericResponse response = new GenericResponse();
		response.setStatus(Status.FAILURE);
		response.setErrorCode(ErrorCode.GENERIC_EXCEPTION.getErrorCode());
		response.setErrorMessage(ErrorCode.GENERIC_EXCEPTION.getErrorMessage());
		return response;
	}

}
