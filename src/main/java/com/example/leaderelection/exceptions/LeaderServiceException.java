package com.example.leaderelection.exceptions;

import lombok.Getter;

@Getter
public class LeaderServiceException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String errorCode;
	String errorMessage;
	
	public LeaderServiceException(String errorCode,String errorMessage){
		super(String.format("%s : %s",errorCode,errorMessage));
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

}
