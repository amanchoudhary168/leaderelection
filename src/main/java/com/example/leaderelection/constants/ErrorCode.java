package com.example.leaderelection.constants;

public enum ErrorCode {
	NO_NODE_FOUND("LS:001","No nodes are present to elect the leader"),
	NO_LEADER_EXCEPTION("LS:002","Currently no leaders are present"),
	GENERIC_EXCEPTION("LS:003","Something went wrong.");

	private String errorCode;
	private String errorMessage;

	private ErrorCode(String errorCode,String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;

	}
	public String getErrorCode(){
		return errorCode;
	}

	public String getErrorMessage() {
		return this.errorMessage; 
	}

}

