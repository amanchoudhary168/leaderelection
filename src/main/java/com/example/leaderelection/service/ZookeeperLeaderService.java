package com.example.leaderelection.service;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;

public interface ZookeeperLeaderService {
	
	public void volunterForLeadership() throws KeeperException, InterruptedException;
	
	public void electLeader() throws KeeperException,InterruptedException;
	
	public void relectLeader(WatchedEvent event) throws KeeperException,InterruptedException;
	
	public String getCurrentLeader()throws KeeperException,InterruptedException;
	

}
