package com.example.leaderelection.service.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.leaderelection.constants.ErrorCode;
import com.example.leaderelection.exceptions.LeaderServiceException;
import com.example.leaderelection.listeners.ZookeeperLeaderWatcher;
import com.example.leaderelection.model.Nodes;
import com.example.leaderelection.service.ZookeeperLeaderService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * This is a service which provides below mentioned services:
 * <ul>
 * 	<li>Add a node for leader election.</li>
 * 	<li>Elect a node as leader from the list volunteered nodes.</li>
 * 	<li>Reelect the leader in case of leader crashed</li>
 * </ul>
 * 
 * @author aman.choudhary
 * @since 02/07/2021
 * @version : 1.0.0
 *  * **/

@Slf4j
@Service
public class ZookeeperLeaderServiceImpl  implements ZookeeperLeaderService{


	private static final String  ELECTION_NAMESPACE="/leader-election";

	@Autowired
	ZooKeeper zooKeeper;

	@Autowired
	ZookeeperLeaderWatcher leaderWatcher;

	@Override
	public void volunterForLeadership() throws KeeperException, InterruptedException {
		String zookeeperPrefix = ELECTION_NAMESPACE+"/s_";
		Stat stat = zooKeeper.exists(ELECTION_NAMESPACE,false);
		if(stat == null)
			zooKeeper.create(ELECTION_NAMESPACE, new byte[]{}, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		String zNodeFullPath = zooKeeper.create(zookeeperPrefix, new byte[] {},
				ZooDefs.Ids.OPEN_ACL_UNSAFE ,CreateMode.EPHEMERAL_SEQUENTIAL);
		log.info("Node created in zookeeper ={}",zNodeFullPath);
		Nodes.INSTANCE.addNode(zNodeFullPath.replace(ELECTION_NAMESPACE+"/",""));
		try{
			getCurrentLeader();
		}catch (LeaderServiceException e) {
			log.info("Currently no leader is present.Electing a new leader");
			electLeader();
		}



	}

	@Override
	public void electLeader() throws KeeperException, InterruptedException {
		List<String> nodes = zooKeeper.getChildren(ELECTION_NAMESPACE, false);
		if(nodes == null) {
			log.error("No nodes are available to elect leader.");
			throw new LeaderServiceException(ErrorCode.NO_NODE_FOUND.getErrorCode(),
					ErrorCode.NO_NODE_FOUND.getErrorMessage());
		}
		Collections.sort(nodes);
		if(StringUtils.isEmpty(Nodes.INSTANCE.getCurrentLeader())) {
			String currentNode = nodes.get(0).replace(ELECTION_NAMESPACE+"/","");
			Nodes.INSTANCE.setCurrentLeader(currentNode);
			if(!Nodes.INSTANCE.getNodes().contains(currentNode)) {
				Nodes.INSTANCE.addNode(currentNode);
			}
		}

		if(Nodes.INSTANCE.getCurrentLeader() == nodes.get(0)) {
			log.info("The current leader is {}",Nodes.INSTANCE.getCurrentLeader());
		}
		zooKeeper.exists(ELECTION_NAMESPACE+"/"+Nodes.INSTANCE.getCurrentLeader(),leaderWatcher);

	}

	@Override
	public void relectLeader(WatchedEvent event) throws KeeperException, InterruptedException{
		String affectedNode = event.getPath().replace(ELECTION_NAMESPACE+"/", "");
		log.info("One of the node has crashed : {}",affectedNode);
		if(!((Nodes.INSTANCE.getCurrentLeader()).equals(affectedNode))) {
			log.info("No change in the leader");
			Nodes.INSTANCE.getNodes().remove(affectedNode);
			return;
		}
		log.info("Electing leader again.");
		Nodes.INSTANCE.setCurrentLeader(StringUtils.EMPTY);
		electLeader();
	}
	
	@Override
	public String getCurrentLeader() throws KeeperException,InterruptedException{
		String currentLeader = Nodes.INSTANCE.getCurrentLeader();
		if(!(StringUtils.EMPTY).equals(currentLeader)) {
			Stat status = zooKeeper.exists(ELECTION_NAMESPACE+"/"+currentLeader, true);
			if(status == null) {
				throw new LeaderServiceException(ErrorCode.NO_LEADER_EXCEPTION.getErrorCode(),
						ErrorCode.NO_LEADER_EXCEPTION.getErrorMessage());
			}
		}else {
			throw new LeaderServiceException(ErrorCode.NO_LEADER_EXCEPTION.getErrorCode(),
					ErrorCode.NO_LEADER_EXCEPTION.getErrorMessage());
		}
		return currentLeader;
	}

}
