package com.example.leaderelection.listeners;

import java.util.UUID;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.leaderelection.service.ZookeeperLeaderService;
import lombok.extern.slf4j.Slf4j;

/**
 * This is a watcher which will watch events coming from ZooKeeper.
 * Based on the type of events it will be taking the decisions.
 * 
 * 
 * @author aman.choudhary
 * @since 02/07/2021
 * @version 1.0.0
 * 
 * **/

@Slf4j
public class ZookeeperLeaderWatcher implements Watcher {
	
	private final String EVENT_ID_KEY = "event.id";
	
	@Autowired
	ZookeeperLeaderService zookeeperLeaderService;

	@Override
	public void process(WatchedEvent event) {
		String eventId = UUID.randomUUID().toString();
		MDC.put(EVENT_ID_KEY, eventId);
		log.info("Event received in the watcher.");
		switch(event.getType()) {
		
			case None :
				if(event.getState() == Event.KeeperState.SyncConnected)
					log.info("Connected to zookeeper successfully.");
				break;
			case NodeDeleted : 
				try {
					log.info("Going to reelect the leader,if required.");
					zookeeperLeaderService.relectLeader(event);
				}catch(Exception e ) {
					log.error("Exception occured while relecting the leader.");
				}
				break;
			case NodeCreated:
				try {
					log.info("Going to elect the leader,if required.");
					zookeeperLeaderService.electLeader();
				}catch(Exception e) {
					log.error("Exception occured while electing the leader");
				}
				break;
			case NodeDataChanged:
				log.info("Leader updated progress of task");
			default:
				
		}
		
		
		
	}

}
