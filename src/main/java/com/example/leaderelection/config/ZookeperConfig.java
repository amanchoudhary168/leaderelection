package com.example.leaderelection.config;

import java.io.IOException;

import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.leaderelection.listeners.ZookeeperLeaderWatcher;

@Configuration
public class ZookeperConfig {

	@Autowired
	ZookeeperProperties zookeeperProp;
	
	
	@Bean
	public ZookeeperLeaderWatcher getWatcher() {
		return new ZookeeperLeaderWatcher();
	}
	
	@Bean
	public ZooKeeper getZookeeperInstance() throws IOException {
		ZooKeeper instance = new ZooKeeper(zookeeperProp.getHost(),zookeeperProp.getTimeout(),getWatcher());
		return instance;
		
		
		
	}
	
}
