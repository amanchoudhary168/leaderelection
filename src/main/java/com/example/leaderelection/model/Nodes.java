package com.example.leaderelection.model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang.StringUtils;



public enum Nodes {
	INSTANCE;
	
	private List<String> nodeList = new CopyOnWriteArrayList<>();
	private String currentLeader = StringUtils.EMPTY;
	
	public void addNode(String name) {
		nodeList.add(name);
	}
	
	public void setCurrentLeader(String nodeName) {
		 this.currentLeader = nodeName;
	}
	
	public String getCurrentLeader() {
		return this.currentLeader;
	}
	
	public List<String> getNodes(){
		return nodeList;
	}
	

}
