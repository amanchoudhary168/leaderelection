package com.example.leaderelection.model;

import com.example.leaderelection.constants.Status;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class GenericResponse {
	Status status;
	String errorCode;
	String errorMessage;
	
}
