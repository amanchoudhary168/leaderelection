<h3>Description</h3>
This projects the demonstrate the implementation of leader election algorith in a distributed system with the help of zookeeper.

<h3>Technology Used</h3>
    <ul>
        <li>SpringBoot</li>
        <li>Java 8</li>
        <li>Zookeeper</li>
    </ul>
<h3>Setup:</h3>
<ul>Change the application.yml,add the zookeeper port on which it is exposed here</ul>
<h3>How to run the project</h3>
    <ul>
        <li> Take the checkout and provide the zookeeper endpoint</li>
        <li>Start the application as a spring boot project in any of IDE of your preference</li>
        <li>By Default application runs on port 8082.Make a post call to endpoint </volunter-for-leadership> to register your instance on zookeeper</li>
        <li>To find the current leader in the ecosystem use METHOD:GET </current-leader</li>
    </ul>
<h3>How to verify.</h3>
Open zookeeper using zookeeper-cli and list all the available nodes using command ls /.<br>
Once you have listed out the nodes,please list sub-directories of /leader-election node.<br>
After sublisting you will find an individual node for each registered server.Among the registered nodes in the zookeeper,one of them  will be serving as a leader .
